package com.alphasquad.blindtest.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Player {

    private String id;
    private String username;
    private String email;
    private PlayerRole role;

}
