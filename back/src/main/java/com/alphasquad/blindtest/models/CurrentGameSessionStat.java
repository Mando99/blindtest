package com.alphasquad.blindtest.models;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CurrentGameSessionStat {

    private Player player;
    private int playerScore;

    public CurrentGameSessionStat(Player player, int playerScore) {
        this.player = player;
        this.playerScore = playerScore;
    }
}
