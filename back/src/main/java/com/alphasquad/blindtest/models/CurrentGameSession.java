package com.alphasquad.blindtest.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class CurrentGameSession {

    private String gameID;
    private String currentImageID;
    private String currentImage;
    private String question;
    private String playerAnswer;
    private volatile boolean answerHasBeenFound;
    private boolean gameIsFinish;
    private boolean gameStarted;
    private List<CurrentGameSessionStat> currentGameSessionStats = new ArrayList<>();

}
