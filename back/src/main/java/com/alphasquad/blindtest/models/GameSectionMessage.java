package com.alphasquad.blindtest.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class GameSectionMessage {

    private String content;
    private String addedAt;
    private Player player;

}
