package com.alphasquad.blindtest.core.server;

import com.alphasquad.blindtest.core.iomessage.OMessage;
import com.alphasquad.blindtest.utils.UIOMessage;
import com.alphasquad.blindtest.utils.UWebSocket;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.*;
import java.util.UUID;

@Getter
@Setter
@ToString
public class Session {

    private String id;

    private final OutputStream outputStream;

    public Session(OutputStream out) {
        this.id = UUID.randomUUID().toString();
        this.outputStream = out;
    }

    public void sendOMessage(OMessage oMessage) {
        try {
            this.outputStream.write(UWebSocket.encodeFrame(UIOMessage.encode(oMessage)));
            this.outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
