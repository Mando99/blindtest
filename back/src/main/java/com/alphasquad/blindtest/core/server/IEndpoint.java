package com.alphasquad.blindtest.core.server;

import com.alphasquad.blindtest.core.iomessage.IMessage;

import javax.mail.MessagingException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public interface IEndpoint {

    void onOpen(Session session);

    void onMessage(Session session, IMessage iMessage) throws InvocationTargetException, IllegalAccessException, IOException, MessagingException, InterruptedException;

    void onClose(Session session);

    void onError(Session session, Throwable throwable);

}
