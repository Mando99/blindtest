package com.alphasquad.blindtest.core.iomessage;

public enum IOMessageCommand {
    GET_GAMES,

    CREATE_SECTION,
    UPDATE_SESSION,
    INVITE_PLAYER_IN_SECTION,
    JOIN_SECTION,
    SEND_MESSAGE_TO_SECTION,
    GET_GAME_SECTION,
    UPDATE_SECTION,
    REMOVE_PLAYER,
    LEAVE_SECTION,

    CHOOSE_GAME,
    START_GAME,
    SEND_ANSWER,
    JOIN_GAME,
    SEND_FILE,
    NEXT_IMAGE
}
