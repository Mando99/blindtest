package com.alphasquad.blindtest.core.config;

import com.alphasquad.blindtest.utils.UFilesResources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GlobalConfig {

    private static GlobalConfig INSTANCE;

    private final Properties properties;

    private GlobalConfig(String propertiesFileName) throws IOException {
        this.properties = new Properties();
        InputStream is = UFilesResources.getInputStreamFile(propertiesFileName);
        this.load(is);
    }

    public static GlobalConfig get(String propertiesFileName) throws IOException {
        if (GlobalConfig.INSTANCE == null) {
            return GlobalConfig.INSTANCE = new GlobalConfig(propertiesFileName);
        }

        return GlobalConfig.INSTANCE;
    }

    public Properties getProperties() {
        return this.properties;
    }

    public String getProperty(String property) {
        return this.properties.getProperty(property);
    }

    private void load(InputStream is) throws IOException {
        try (is) {
            this.properties.load(is);
            System.out.println("Application Properties loaded !");
        }
    }

}
