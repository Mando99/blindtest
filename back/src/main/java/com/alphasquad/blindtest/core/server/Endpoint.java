package com.alphasquad.blindtest.core.server;

import com.alphasquad.blindtest.controllers.GameController;
import com.alphasquad.blindtest.controllers.GameSectionController;
import com.alphasquad.blindtest.controllers.SecurityController;
import com.alphasquad.blindtest.core.iomessage.IMessage;
import com.alphasquad.blindtest.models.Player;

import javax.mail.MessagingException;
import java.io.IOException;

public class Endpoint implements IEndpoint {

    private final GameController gameController = new GameController();
    private final GameSectionController gameSectionController = new GameSectionController();
    private final SecurityController securityController = new SecurityController();

    public static Endpoint get() {
        return Endpoint.Instance.INSTANCE;
    }

    @Override
    public void onOpen(Session session) {
        System.out.println("Endpoint : onOpen() -> session => " + session);
    }

    @Override
    public void onMessage(Session session, IMessage iMessage) throws MessagingException, IOException, InterruptedException {
        // Not in game section
        switch (iMessage.getCommand()) {
            case CREATE_SECTION -> this.gameSectionController.createSection(session, iMessage.getGameSection(), iMessage.getPlayer());
            case JOIN_SECTION -> this.gameSectionController.joinSection(session, iMessage.getPlayer(), iMessage.getGameSection().getSecretCode());
            default -> {
                Player playerAuthorized = this.securityController.getPlayerIfIsAuthorized(iMessage.getGameSection().getSecretCode(), iMessage.getPlayerWhoSendMessageID(), session);
                System.out.println("Endpoint : onMessage() -> playerAuthorized => " + playerAuthorized);
                if (playerAuthorized != null) {
                    // In game section
                    switch (iMessage.getCommand()) {
                        case GET_GAME_SECTION -> this.gameSectionController.getGameSection(session, iMessage.getGameSection().getSecretCode(), playerAuthorized);
                        case INVITE_PLAYER_IN_SECTION -> this.gameSectionController.invitePlayerInSection(session, iMessage.getGameSection().getSecretCode(), iMessage.getPlayer().getEmail());
                        case UPDATE_SECTION -> this.gameSectionController.updateGameSection(iMessage.getGameSection().getSecretCode(), iMessage.getGameSection().getName(), playerAuthorized);
                        case SEND_MESSAGE_TO_SECTION -> this.gameSectionController.sendMessageToSection(iMessage.getGameSection().getSecretCode(), iMessage.getGameSectionMessage(), playerAuthorized);
                        case LEAVE_SECTION -> this.gameSectionController.leaveSection(iMessage.getGameSection().getSecretCode(), playerAuthorized);
                        case REMOVE_PLAYER -> this.gameSectionController.removePlayer(iMessage.getGameSection().getSecretCode(), iMessage.getPlayer(), playerAuthorized);
                        case GET_GAMES -> this.gameController.getGames(session);
                        case CHOOSE_GAME -> this.gameController.chooseGame(iMessage.getGameSection().getSecretCode(), iMessage.getGame().getId(), playerAuthorized);
                        case JOIN_GAME -> this.gameController.joinGame(iMessage.getGameSection().getSecretCode(), iMessage.getGame().getId(), playerAuthorized);
                        case START_GAME -> this.gameController.startGame(iMessage.getGameSection().getSecretCode(), iMessage.getGame().getId(), playerAuthorized);
                        case SEND_ANSWER -> this.gameController.sendAnswer(session, iMessage.getGameSection().getSecretCode(), iMessage.getCurrentGameSession(), playerAuthorized);
                        case NEXT_IMAGE -> this.gameController.nextImage(iMessage.getGameSection().getSecretCode(), iMessage.getCurrentGameSession(), playerAuthorized);
                    }
                }
            }
        }
    }

    @Override
    public void onClose(Session session) {
        System.out.println("Endpoint : onClose() -> session => " + session);
    }

    @Override
    public void onError(Session session, Throwable throwable) {
        System.out.println("Endpoint : onError() -> session => " + session);
        System.out.println("Endpoint : onError() -> throwable => " + throwable);
    }

    private static class Instance {
        private static final Endpoint INSTANCE = new Endpoint();
    }

}
