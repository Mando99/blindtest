package com.alphasquad.blindtest.controllers;

import com.alphasquad.blindtest.core.iomessage.IOMessageCommand;
import com.alphasquad.blindtest.core.iomessage.OMessage;
import com.alphasquad.blindtest.core.iomessage.OMessageStatus;
import com.alphasquad.blindtest.core.server.Server;
import com.alphasquad.blindtest.core.server.Session;
import com.alphasquad.blindtest.models.*;

import java.util.*;

public class GameController {

    public void getGames(Session session) {
        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.GET_GAMES);
        oMessage.setStatus(OMessageStatus.SUCCESS);
        oMessage.setGames(new ArrayList<>(Server.games.values()));
        session.sendOMessage(oMessage);
    }

    public void chooseGame(String secretCode, String gameID, Player player) {
        GameSection section = Server.gameSections.get(secretCode);
        Game game = Server.games.get(gameID);

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.CHOOSE_GAME);

        if (section != null && game != null && player.getRole() == PlayerRole.CREATOR) {
            section.setCurrentGameSession(null);

            oMessage.setStatus(OMessageStatus.SUCCESS);
            oMessage.setGame(game);

            CurrentGameSession currentGameSession = new CurrentGameSession();
            currentGameSession.setGameID(gameID);
            currentGameSession.setGameIsFinish(false);
            currentGameSession.setGameStarted(false);
            currentGameSession.getCurrentGameSessionStats().add(new CurrentGameSessionStat(player, 0));

            section.setCurrentGameSession(currentGameSession);
            oMessage.setCurrentGameSession(currentGameSession);

            Server.broadcastToPlayersInSection(secretCode, oMessage);
        } else {
            oMessage.setStatus(OMessageStatus.ERROR);
        }
    }

    public void joinGame(String secretCode, String gameID, Player player) {
        GameSection section = Server.gameSections.get(secretCode);
        Game game = Server.games.get(gameID);

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.JOIN_GAME);

        if (section != null && game != null && player.getRole() == PlayerRole.GUEST) {
            for (CurrentGameSessionStat currentGameSessionStats : section.getCurrentGameSession().getCurrentGameSessionStats()) {
                if (currentGameSessionStats.getPlayer().getId().equals(player.getId())) {
                    return;
                }
            }

            CurrentGameSessionStat currentGameSessionStat = new CurrentGameSessionStat(player, 0);
            section.getCurrentGameSession().getCurrentGameSessionStats().add(currentGameSessionStat);
            oMessage.setCurrentGameSessionStat(currentGameSessionStat);
            oMessage.setCurrentGameSession(section.getCurrentGameSession());

            oMessage.setPlayer(player);
            oMessage.setStatus(OMessageStatus.SUCCESS);
            Server.broadcastToPlayersInSection(secretCode, oMessage);
        } else {
            oMessage.setStatus(OMessageStatus.ERROR);
        }
    }

    public void startGame(String secretCode, String gameID, Player player) throws InterruptedException {
        GameSection section = Server.gameSections.get(secretCode);
        Game game = Server.games.get(gameID);

        List<GameBlindTest> images = Server.gameBlindTests.get(gameID);

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.START_GAME);

        if (section != null && game != null && images != null && player.getRole() == PlayerRole.CREATOR) {
            oMessage.setStatus(OMessageStatus.SUCCESS);
            oMessage.setGame(game);

            for (CurrentGameSessionStat currentGameSessionStat : section.getCurrentGameSession().getCurrentGameSessionStats()) {
                currentGameSessionStat.setPlayerScore(0);
            }

            GameBlindTest firstImage = images.get(0);
            boolean allPlayersReceivedFirstImage = Server.sendFile(firstImage.getFileURL(), secretCode, null);

            if (allPlayersReceivedFirstImage) {
                section.getCurrentGameSession().setCurrentImageID(firstImage.getId());
                section.getCurrentGameSession().setQuestion(firstImage.getQuestion());
                section.getCurrentGameSession().setAnswerHasBeenFound(false);
                section.getCurrentGameSession().setGameIsFinish(false);
                section.getCurrentGameSession().setGameStarted(true);
                oMessage.setCurrentGameSession(section.getCurrentGameSession());
                Server.broadcastToPlayersInSection(secretCode, oMessage);
            }
        }
    }

    public void sendAnswer(Session session, String secretCode, CurrentGameSession currentGameSession, Player player) {
        GameSection section = Server.gameSections.get(secretCode);
        Game game = Server.games.get(currentGameSession.getGameID());

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.SEND_ANSWER);

        if (section != null && game != null) {
            if (section.getCurrentGameSession().getCurrentImageID().equals(currentGameSession.getCurrentImageID())) {
                List<GameBlindTest> images = Server.gameBlindTests.get(currentGameSession.getGameID());

                GameBlindTest gameBlindTest = null;
                for (GameBlindTest gi : images) {
                    if (gi.getId().equals(currentGameSession.getCurrentImageID())) {
                        gameBlindTest = gi;
                        break;
                    }
                }

                if (gameBlindTest != null && gameBlindTest.getAnswer() != null && gameBlindTest.getAnswer().equalsIgnoreCase(currentGameSession.getPlayerAnswer().trim())) {
                    oMessage.setGameAnswer(gameBlindTest.getAnswer());
                    oMessage.setStatus(OMessageStatus.SUCCESS);
                    oMessage.setGame(game);

                    int indexCurrentImage = images.indexOf(gameBlindTest);
                    if (indexCurrentImage + 1 >= images.size()) {
                        section.getCurrentGameSession().setGameIsFinish(true);
                        section.getCurrentGameSession().setGameStarted(false);
                    }

                    for (CurrentGameSessionStat currentGameSessionStat : section.getCurrentGameSession().getCurrentGameSessionStats()) {
                        if (currentGameSessionStat.getPlayer().getId().equals(player.getId())) {
                            currentGameSessionStat.setPlayerScore(currentGameSessionStat.getPlayerScore() + 1);
                            section.getCurrentGameSession().setAnswerHasBeenFound(true);
                            oMessage.setCurrentGameSession(section.getCurrentGameSession());
                            oMessage.setPlayer(player);
                            Server.broadcastToPlayersInSection(secretCode, oMessage);
                            break;
                        }
                    }
                } else {
                    oMessage.setStatus(OMessageStatus.ERROR);
                    session.sendOMessage(oMessage);
                }
            }
        }
    }

    public void nextImage(String secretCode, CurrentGameSession currentGameSession, Player player) {
        GameSection section = Server.gameSections.get(secretCode);
        Game game = Server.games.get(currentGameSession.getGameID());

        List<GameBlindTest> images = Server.gameBlindTests.get(currentGameSession.getGameID());

        OMessage oMessage = new OMessage();
        oMessage.setCommand(IOMessageCommand.NEXT_IMAGE);

        if (section != null && game != null && images != null && player.getRole() == PlayerRole.CREATOR) {
            oMessage.setStatus(OMessageStatus.SUCCESS);
            oMessage.setGame(game);

            GameBlindTest currentImage = null;
            for (GameBlindTest gameBlindTest : images) {
                if (gameBlindTest.getId().equals(currentGameSession.getCurrentImageID())) {
                    currentImage = gameBlindTest;
                    break;
                }
            }

            GameBlindTest nextImage = null;
            if (currentImage != null) {
                int indexCurrentImage = images.indexOf(currentImage);
                if (indexCurrentImage + 1 < images.size()) {
                    nextImage = images.get(indexCurrentImage + 1);
                }
            }

            if (nextImage != null) {
                section.getCurrentGameSession().setCurrentImageID(nextImage.getId());
                section.getCurrentGameSession().setAnswerHasBeenFound(false);
                section.getCurrentGameSession().setQuestion(nextImage.getQuestion());
                oMessage.setCurrentGameSession(section.getCurrentGameSession());
                Server.broadcastToPlayersInSection(secretCode, oMessage);
                Server.sendFile(nextImage.getFileURL(), secretCode, null);
            } else {
                section.getCurrentGameSession().setGameIsFinish(true);
                section.getCurrentGameSession().setGameStarted(false);
                oMessage.setCurrentGameSession(section.getCurrentGameSession());
                Server.broadcastToPlayersInSection(secretCode, oMessage);
            }
        }
    }

}
