package com.alphasquad.blindtest.utils;

import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class UJsonFile {

    private static final Gson gson = new Gson();

    public static <T> List<T> toObjects(String filename, String key, Class<T> obj) throws IOException {
        String s = IOUtils.toString(UFilesResources.getInputStreamFile(filename), StandardCharsets.UTF_8);

        JSONObject json = new JSONObject(s);
        JSONArray jsonArray = json.getJSONArray(key);

        List<T> arr = new ArrayList<>();
        jsonArray.forEach(j -> {
            T o = gson.fromJson(j.toString(), obj);
            arr.add(o);
        });

        return arr;
    }

}
