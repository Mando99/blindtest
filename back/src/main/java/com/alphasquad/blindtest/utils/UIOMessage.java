package com.alphasquad.blindtest.utils;

import com.alphasquad.blindtest.core.iomessage.IMessage;
import com.alphasquad.blindtest.core.iomessage.OMessage;
import com.google.gson.Gson;

public class UIOMessage {

    private static final Gson gson = new Gson();

    public static IMessage decode(String iMessage) {
        System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("IMessage => " + iMessage);
        IMessage iMessageDecoded = gson.fromJson(iMessage, IMessage.class);
        System.out.println("IMessage decoded => " + iMessageDecoded);
        return iMessageDecoded;
    }

    public static String encode(OMessage oMessage) {
        System.out.println("OMessage => " + oMessage);
        String oMessageEncoded = gson.toJson(oMessage);
        System.out.println("OMessage encoded => " + oMessageEncoded);
        return oMessageEncoded;
    }

}
