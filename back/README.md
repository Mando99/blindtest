# Back - BlindTest

## Exigences

- Maven (https://maven.apache.org/)
- Mysql (https://www.mysql.com/)

## Exécuter

```sh
cd back
```

```sh
mvn clean install
```

```sh
java -jar target/SlackLike-1.0-SNAPSHOT.jar
```

## TEST ENDPOINTS

```sh
npm install -g wscat
```
```sh
wscat -c "ws://127.0.0.1:8080/"
```

#### Exemple

```sh
>  {"command":"GET_GAME_SECTION","sessionID":"ace3136f-1b36-4142-8806-de8600077a96","gameSection":{"secretCode":"016c5aa8-b513-40e4-b231-edc18a962e46"},"playerWhoSendMessageID":"a5b8d48d-b0c9-45d7-8b53-807c32638c69"}
```
