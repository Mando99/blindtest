import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {GameSectionType} from "../../../types/models/game-section.type";
import {IOMessageType} from "../../../types/iomessage/IOMessage.type";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";

@Component({
  selector: 'app-invite-player',
  templateUrl: './invite-player.component.html',
  styleUrls: ['./invite-player.component.css']
})
export class InvitePlayerComponent implements OnInit {
  invitePlayerSectionForm!: FormGroup;

  @Input()
  gameSection!: GameSectionType;

  constructor(private formBuilder: FormBuilder, private webSocketService: WebsocketService) { }

  ngOnInit(): void {
    this.initInvitePlayerSectionForm();
  }

  private initInvitePlayerSectionForm(): void {
    this.invitePlayerSectionForm = this.formBuilder.group({
      email: new FormControl(''),
    });
  }

  onSubmitInvitePlayerSectionForm() {
    if (this.invitePlayerSectionForm.valid) {
      const IMessage: IOMessageType = {
        command: IOMessageCommandEnum.INVITE_PLAYER_IN_SECTION,
        sessionID: localStorage.getItem("session"),
        gameSection: {secretCode: localStorage.getItem("secret_code") || ""},
        playerWhoSendMessageID: localStorage.getItem("player_id") || "",
        player: {email: this.invitePlayerSectionForm.value.email}
      };
      this.webSocketService.sendIMessage(IMessage);
    }
  }

}
