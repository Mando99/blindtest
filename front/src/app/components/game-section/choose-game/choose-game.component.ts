import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {PlayerType} from "../../../types/models/player.type";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {IOMessageType} from "../../../types/iomessage/IOMessage.type";
import {GameType} from "../../../types/models/game.type";

@Component({
  selector: 'app-choose-game',
  templateUrl: './choose-game.component.html',
  styleUrls: ['./choose-game.component.css']
})
export class ChooseGameComponent implements OnInit {

  @Input()
  authPlayer!: PlayerType;

  @Input()
  games: GameType[] | undefined = [];

  @Output()
  gameChosen: EventEmitter<GameType> = new  EventEmitter();

  constructor(private webSocketService: WebsocketService) { }

  ngOnInit(): void {
    this.getGames();
  }

  chooseGame(game: GameType): void {
    const IMessage: IOMessageType = {
      command: IOMessageCommandEnum.CHOOSE_GAME,
      sessionID: localStorage.getItem("session"),
      gameSection: { secretCode: localStorage.getItem("secret_code") || "" },
      playerWhoSendMessageID: localStorage.getItem("player_id") || "",
      game: { id: game.id }
    };
    this.webSocketService.sendIMessage(IMessage);
    this.gameChosen.emit(game);
  }

  private getGames() {
    const IMessage: IOMessageType = {
      command: IOMessageCommandEnum.GET_GAMES,
      sessionID: localStorage.getItem("session"),
      gameSection: { secretCode: localStorage.getItem("secret_code") || "" },
      playerWhoSendMessageID: localStorage.getItem("player_id") || ""
    };
    this.webSocketService.sendIMessage(IMessage);
  }

}
