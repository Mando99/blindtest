import {Component, Input, OnInit} from '@angular/core';
import {GameType} from 'src/app/types/models/game.type';
import {WebsocketService} from "../../../services/websocket/websocket.service";
import {IOMessageType} from "../../../types/iomessage/IOMessage.type";
import {IOMessageCommandEnum} from "../../../enums/i-o-message-command.enum";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {CurrentGameSessionType} from "../../../types/models/current-game-session.type";
import {SafeUrl} from "@angular/platform-browser";
import {PlayerType} from 'src/app/types/models/player.type';
import {CurrentGameSessionStatType} from "../../../types/models/current-game-session-stat.type";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  @Input()
  authPlayer!: PlayerType;

  @Input()
  game!: GameType;

  @Input()
  gameStarted!: boolean;

  @Input()
  currentGameSession!: CurrentGameSessionType;

  @Input()
  fileURL!: SafeUrl;

  @Input()
  fileType!: string;

  @Input()
  fileURLStatus!: boolean;

  public answerForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private webSocketService: WebsocketService) {
  }

  ngOnInit(): void {
    this.initAnswerForm();
  }

  private initAnswerForm(): void {
    this.answerForm = this.formBuilder.group({
      answer: new FormControl('')
    });
  }

  playerAlreadyJoinGame(): boolean {
    let playerExist: boolean = false;
    this.currentGameSession.currentGameSessionStats?.forEach(currentGameSessionStat => {
      if (currentGameSessionStat.player?.id == this.authPlayer.id) {
        playerExist = true
      }
    });
    return playerExist;
  }

  joinGame(game: GameType): void {
    const IMessage: IOMessageType = {
      command: IOMessageCommandEnum.JOIN_GAME,
      sessionID: localStorage.getItem("session"),
      gameSection: {secretCode: localStorage.getItem("secret_code") || ""},
      playerWhoSendMessageID: localStorage.getItem("player_id") || "",
      game: {id: game.id}
    };
    this.webSocketService.sendIMessage(IMessage);
  }

  startGame(): void {
    this.fileURL = "";
    this.gameStarted = true;
    const IMessage: IOMessageType = {
      command: IOMessageCommandEnum.START_GAME,
      gameSection: {secretCode: localStorage.getItem("secret_code") || ""},
      playerWhoSendMessageID: localStorage.getItem("player_id") || "",
      game: {id: this.game.id}
    };
    this.webSocketService.sendIMessage(IMessage);
  }

  sendAnswer(): void {
    if (this.answerForm.valid) {
      const iMessage: IOMessageType = {
        command: IOMessageCommandEnum.SEND_ANSWER,
        game: this.game,
        gameSection: {secretCode: localStorage.getItem("secret_code") || ""},
        playerWhoSendMessageID: localStorage.getItem("player_id") || "",
        currentGameSession: {...this.currentGameSession, playerAnswer: this.answerForm.value.answer}
      };
      this.webSocketService.sendIMessage(iMessage);
      this.answerForm.reset();
    }
  }

  nextImage(): void {
    const iMessage: IOMessageType = {
      command: IOMessageCommandEnum.NEXT_IMAGE,
      game: this.game,
      gameSection: {secretCode: localStorage.getItem("secret_code") || ""},
      playerWhoSendMessageID: localStorage.getItem("player_id") || "",
      currentGameSession: this.currentGameSession
    };
    this.webSocketService.sendIMessage(iMessage);
  }

  sort(currentGameSessionStats: CurrentGameSessionStatType[]): CurrentGameSessionStatType[] {
    return currentGameSessionStats.sort((a, b) => b.playerScore - a.playerScore);
  }

}
