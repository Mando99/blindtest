import {Component, OnInit} from '@angular/core';
import {GameSectionType} from "../../types/models/game-section.type";
import {AlertType} from "../../types/alert/alert.type";
import {Subscription} from "rxjs";
import {OMessageStatusEnum} from "../../enums/o-message-status.enum";
import {IOMessageCommandEnum} from "../../enums/i-o-message-command.enum";
import {WebsocketService} from "../../services/websocket/websocket.service";
import {IOMessageType} from "../../types/iomessage/IOMessage.type";
import {PlayerType} from "../../types/models/player.type";
import {SecurityService} from "../../services/security/security.service";
import {GameType} from 'src/app/types/models/game.type';
import {CurrentGameSessionType} from "../../types/models/current-game-session.type";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";
import {SendFileStatus} from "../../enums/send-file-status.enum";

@Component({
  selector: 'app-game-section',
  templateUrl: './game-section.component.html',
  styleUrls: ['./game-section.component.css']
})
export class GameSectionComponent implements OnInit {

  modalOpen = true;

  modalIsOpen = false;
  modalContent = "";

  gameSection!: GameSectionType;
  authPlayer!: PlayerType;

  showAlert: boolean = false;
  alert!: AlertType;
  successMessage: string = "SUCCESS";
  errorMessage: string = "ERROR something went wrong try again.";

  gameChosen!: GameType;

  gameStarted: boolean = false;

  currentGameSession!: CurrentGameSessionType;

  fileURL: SafeUrl = "";
  fileType: string = "image";
  fileURLStatus: boolean = false;

  private webSocketSubscription!: Subscription;
  secretCode: string | null = localStorage.getItem("secret_code");

  constructor(private webSocketService: WebsocketService, private securityService: SecurityService, private domSanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.subscriptionWebSocket();
    this.getGameSection();
  }

  private subscriptionWebSocket(): void {
    this.webSocketSubscription = this.webSocketService.getWebSocket().subscribe(msg => {
      console.log('message received: ' + JSON.stringify(msg));
      if (msg.status == OMessageStatusEnum.SUCCESS) {
        if (msg.command == IOMessageCommandEnum.UPDATE_SESSION) {
          this.securityService.setSessionInLocalStorage(msg.sessionID);
        }

        if (msg.command == IOMessageCommandEnum.GET_GAME_SECTION) {
          this.fileURL = '';
          this.fileURLStatus = false;
          this.gameSection = msg.section;
          this.authPlayer = msg.player;
          if (msg.currentGameSession) {
            this.currentGameSession = msg.currentGameSession;
            this.gameStarted = msg.currentGameSession.gameStarted;
            if (msg.currentGameSession.gameID) {
              this.gameChosen = msg.game;
            }
          }
        }

        if (msg.command == IOMessageCommandEnum.UPDATE_SECTION) {
          if (msg.playerWhoSendMessageID == localStorage.getItem("player_id")) {
            this.showAlertMsg("SUCCESS", "Game section successfully updated !");
            this.closeModal();
          }
          this.gameSection = msg.section;
        }

        if (msg.command == IOMessageCommandEnum.INVITE_PLAYER_IN_SECTION) {
          this.showAlertMsg("SUCCESS", "Email successfully sent !");
          this.closeModal();
        }

        if (msg.command == IOMessageCommandEnum.SEND_MESSAGE_TO_SECTION) {
          this.gameSection.messages?.push(msg.gameSectionMessage);
        }

        if (msg.command == IOMessageCommandEnum.LEAVE_SECTION) {
          if (this.currentGameSession) {
            this.currentGameSession.currentGameSessionStats = this.currentGameSession.currentGameSessionStats?.filter(currentGameSessionStat => currentGameSessionStat.player?.id != msg.playerWhoSendMessageID);
          }
          this.showAlertMsg("SUCCESS", msg.player?.username + " left the party !");
          this.gameSection.players = this.gameSection?.players?.filter(player => player.id != msg.playerWhoSendMessageID);
          if (msg.playerWhoSendMessageID == localStorage.getItem("player_id")) {
            this.securityService.removeSessionFromLocalStorage();
            localStorage.removeItem("player_id");
            localStorage.removeItem("secret_code");
          } else if (msg.player?.role == 'CREATOR') {
            this.securityService.removeSessionFromLocalStorage();
            localStorage.removeItem("player_id");
            localStorage.removeItem("secret_code");
          }
          this.showAlertMsg("SUCCESS", msg.player?.username + " left the party !");
        }

        if (msg.command == IOMessageCommandEnum.GET_GAMES) {
          this.gameSection.games = msg.games;
        }

        if (msg.command == IOMessageCommandEnum.JOIN_SECTION) {
          this.gameSection.players?.push(msg.player);
          this.showAlertMsg("SUCCESS", msg.player.username + " join the section !");
        }

        if (msg.command == IOMessageCommandEnum.REMOVE_PLAYER) {
          this.gameSection.players = this.gameSection.players?.filter(player => player.id != msg.player.id);
          if (msg.player.id == localStorage.getItem("player_id")) {
            this.securityService.removeSessionFromLocalStorage();
            localStorage.removeItem("player_id");
            localStorage.removeItem("secret_code");
          }

          this.showAlertMsg("SUCCESS", "The player " + msg.player.username + " has been excluded");
        }

        if (msg.command == IOMessageCommandEnum.CHOOSE_GAME) {
          this.gameChosen = msg.game;
          this.currentGameSession = msg.currentGameSession;
          this.showAlertMsg("SUCCESS", "Game " + this.gameChosen.category + " chosen, you can join the game !");
        }

        if (msg.command == IOMessageCommandEnum.START_GAME) {
          this.currentGameSession = msg.currentGameSession;
          this.showAlertMsg("SUCCESS", "The game started !");
        }

        if (msg.command == IOMessageCommandEnum.SEND_FILE) {
          this.gameStarted = true;
          if (msg.sendFile.order == 0) {
            this.fileType = (msg.sendFile.fileURLPart.startsWith("data:image")) ? "image" : "audio";
          }
          this.fileURL += msg.sendFile.fileURLPart;

          if (msg.sendFile.status == SendFileStatus.END) {
            this.fileURLStatus = true;
            if (typeof this.fileURL === "string") {
              this.fileURL = this.domSanitizer.bypassSecurityTrustResourceUrl(this.fileURL);
            }
          }
        }

        if (msg.command == IOMessageCommandEnum.JOIN_GAME) {
          this.currentGameSession.currentGameSessionStats?.push(msg.currentGameSessionStat);
          if (msg.player.id == localStorage.getItem('player_id')) {
            this.showAlertMsg("SUCCESS", "You join the party !");
          } else {
            this.showAlertMsg("SUCCESS", msg.player.username + " join the party !");
          }
        }

        if (msg.command == IOMessageCommandEnum.SEND_ANSWER) {
          this.currentGameSession = msg.currentGameSession;
          this.currentGameSession.playerAnswer = msg.gameAnswer;
          if (msg.currentGameSession.gameIsFinish) {
            this.gameStarted = msg.currentGameSession.gameStarted;
            this.showAlertMsg("SUCCESS", msg.player.username + " find the good answer ! The game is finish ! ");
          } else {
            this.showAlertMsg("SUCCESS", msg.player.username + " find the good answer !");
          }
        }

        if (msg.command == IOMessageCommandEnum.NEXT_IMAGE) {
          this.fileURL = '';
          this.fileURLStatus = false;
          this.currentGameSession = msg.currentGameSession;
          if (msg.currentGameSession.gameIsFinish) {
            this.gameStarted = msg.currentGameSession.gameStarted;
            this.showAlertMsg("SUCCESS", " The game is finish !");
          } else {
            this.showAlertMsg("SUCCESS", " Next !");
          }
        }
      } else {
        if (msg.sessionID == this.securityService.getSessionFromLocalStorage()) {
          this.showAlertMsg("ERROR");
        }
      }
    }, err => {
      this.leaveSection();
      console.log(err);
      this.securityService.removeSessionFromLocalStorage();
    });
  }

  getGameSection(): void {
    const localStorageSecretCodeKey = localStorage.getItem("secret_code") || "";

    if (localStorageSecretCodeKey) {
      this.webSocketService.sendIMessage({
        command: IOMessageCommandEnum.GET_GAME_SECTION,
        sessionID: this.securityService.getSessionFromLocalStorage(),
        gameSection: {secretCode: localStorageSecretCodeKey},
        playerWhoSendMessageID: localStorage.getItem("player_id") || "",
        currentGameSession: this.currentGameSession
      });
    } else {
      this.securityService.removeSessionFromLocalStorage();
    }
  }

  setChosenGame(game: GameType): void {
    this.gameChosen = game;
    if (game) {
      this.closeModal();
    }
  }

  openModal(modalContent: string): void {
    this.modalContent = modalContent;
    this.modalIsOpen = true;
  }

  closeModal(): void {
    this.modalIsOpen = false;
  }

  leaveSection(): void {
    const iMessageLeaveSection: IOMessageType = {
      command: IOMessageCommandEnum.LEAVE_SECTION,
      sessionID: this.securityService.getSessionFromLocalStorage(),
      gameSection: {
        secretCode: this.gameSection?.secretCode
      },
      playerWhoSendMessageID: localStorage.getItem("player_id") || ""
    };
    this.webSocketService.sendIMessage(iMessageLeaveSection);
  }

  private showAlertMsg(type: "ERROR" | "SUCCESS", personalizedMessage: string = ""): void {
    this.showAlert = true;

    if (type == "SUCCESS") {
      this.alert = {type: "SUCCESS", message: (personalizedMessage == '') ? this.successMessage : personalizedMessage};
    } else {
      this.alert = {type: "ERROR", message: (personalizedMessage == '') ? this.errorMessage : personalizedMessage};
    }

    setTimeout(() => {
      this.showAlert = false;
    }, 6000);
  }

  ngOnDestroy(): void {
    console.log("Game section destroy !");
  }

}
