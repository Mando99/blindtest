import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {AlertType} from "../../types/alert/alert.type";
import {IOMessageType} from "../../types/iomessage/IOMessage.type";
import {IOMessageCommandEnum} from "../../enums/i-o-message-command.enum";

import {WebsocketService} from "../../services/websocket/websocket.service";
import {SecurityService} from "../../services/security/security.service";
import {Subscription} from "rxjs";
import {OMessageStatusEnum} from "../../enums/o-message-status.enum";

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.css']
})
export class PortalComponent implements OnInit {

  public joinSectionForm!: FormGroup;
  public createGameSectionForm!: FormGroup;

  public messageReceived!: IOMessageType;

  public showAlert: boolean = false;
  public alert!: AlertType;
  public errorMessage: string = "ERROR something went wrong try again.";

  localStorageSecretCodeKey: string = "secret_code";
  localStoragePlayerID: string = "player_id";

  private subscription!: Subscription | undefined;

  constructor(private formBuilder: FormBuilder, private webSocketService: WebsocketService, private securityService: SecurityService) {
  }

  private initJoinGameForm(): void {
    this.joinSectionForm = this.formBuilder.group({
      username: new FormControl(''),
      secretCode: new FormControl('')
    });
  }

  private initCreateGameForm(): void {
    this.createGameSectionForm = this.formBuilder.group({
      gameSectionName: new FormControl(''),
      username: new FormControl('')
    });
  }

  ngOnInit(): void {
    this.initJoinGameForm();
    this.initCreateGameForm();

    this.subscription = this.webSocketService.getWebSocket()?.subscribe(msg => {
        this.messageReceived = <IOMessageType>msg;
        console.log('message received : ' + JSON.stringify(this.messageReceived));

        if (this.messageReceived.status == OMessageStatusEnum.SUCCESS) {
          if (this.messageReceived.status == OMessageStatusEnum.SUCCESS || msg.command == IOMessageCommandEnum.JOIN_SECTION) {
            this.securityService.setSessionInLocalStorage(msg.sessionID);
            localStorage.setItem(this.localStorageSecretCodeKey, msg.section.secretCode);
            localStorage.setItem(this.localStoragePlayerID, msg.player.id);
          }
        } else {
          this.alert = {
            type: this.messageReceived.status,
            message: this.errorMessage
          }
        }
      },
      err => {
        this.alert = {
          type: OMessageStatusEnum.ERROR,
          message: this.errorMessage
        }
        console.log(err);
      }
    );
  }

  onSubmitCreateGameForm(): void {
    if (this.createGameSectionForm.valid) {
      const IMessage: IOMessageType = {
        command: IOMessageCommandEnum.CREATE_SECTION,
        sessionID: '',
        gameSection: {name: this.createGameSectionForm.value.gameSectionName},
        player: {username: this.createGameSectionForm.value.username},
      };
      this.webSocketService.sendIMessage(IMessage);
      this.createGameSectionForm.reset();
    }
  }

  onSubmitJoinGameForm(): void {
    if (this.joinSectionForm.valid) {
      const IMessage: IOMessageType = {
        command: IOMessageCommandEnum.JOIN_SECTION,
        sessionID: '',
        player: {username: this.joinSectionForm.value.username},
        gameSection: {secretCode: this.joinSectionForm.value.secretCode}
      };
      this.webSocketService.sendIMessage(IMessage);
      this.joinSectionForm.reset();
    }
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.webSocketService.reConnect();
  }

}
