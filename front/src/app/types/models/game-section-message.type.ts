import {AbstractModelType} from "./abstract-model.type";
import {PlayerType} from "./player.type";

export type GameSectionMessageType = AbstractModelType & {
  content: string,
  addedAt?: string,
  player?: PlayerType
}
