import {AbstractModelType} from "./abstract-model.type";

export type GameBlindTestType = AbstractModelType & {
  fileURL?: string,
  question?: string,
  answer?: string
}
