import {AbstractModelType} from "./abstract-model.type";
import { GameBlindTestType } from "./game-blind-test.type";

export type GameType = AbstractModelType & {
  category?: string,
  images?: GameBlindTestType[]
}
