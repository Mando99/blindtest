import { PlayerType } from "../models/player.type";

export type UserSessionType = {
  player?: PlayerType,
  session?: string | null | undefined
}
